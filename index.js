const { App } = require('@slack/bolt');
const dotenv = require('dotenv');

dotenv.config();

const bot = new App({
  signingSecret: process.env.SLACK_SIGNING_SECRET,
  token: process.env.SLACK_BOT_TOKEN,
});

bot.start(process.env.PORT || 3000).then(() => {
  console.log(`⚡️ Bolt app is running! on ${process.env.PORT || 3000}`)

  bot.event('app_mention', async ({ context, event }) => {
    try {
      handleMention(event);
    }
    catch (e) {
      console.log(`error responding ${e}`);
    }
  });

  bot.action('abort_clean', ({ say, ack, ...data }) => {
    say(`Todo: Handle denial (Set another scheduled message)`);
    ack();
  });

  bot.action('clean_channel', async ({ say, ack, payload }) => {
    console.log('payload', JSON.stringify(payload))
    say(`Let it go !`);
    try {
      await purgeChannel(payload.value);
    } catch (e) {
      console.log(`error responding ${e}`);
      say(`Something went wrong ...`);
    }

    return ack();
  });

  askForClean('test-slack-bot');
}).catch(console.log)

function askForClean(channelId) {
  bot.client.chat.postMessage({
    token: process.env.SLACK_BOT_TOKEN,
    channel: channelId,
    text: 'Remove users form channel ? ',
    blocks: [
      {
        type: 'section',
        text: {
          type: 'mrkdwn',
          text: 'Remove user form channel ?'
        }
      },
      {
        type: 'actions',
        block_id: `choice_${channelId}`,
        elements: [
          {
            type: 'button',
            action_id: 'clean_channel',
            text: {
              type: 'plain_text',
              text: 'Accept'
            },
            style: 'primary',
            value: channelId
          },
          {
            type: 'button',
            action_id: 'abort_clean',
            text: {
              type: 'plain_text',
              text: 'Refuse'
            },
            style: 'danger'
          }
        ]
      }
    ],
  })
}

const userRegexp = /<@(\w+)>\s+(addUser|removeUser)(?:\s*<@(\w+)>\s*)/gi;
const commandRegexp = /<@(\w+)>\s+(listUsers|purgeChannel)/gi;

async function handleMention(event) {
  if (event.text.match(userRegexp)) {
    const [, botId, command, userId] = userRegexp.exec(event.text)
    switch (command) {
      case 'addUser':
        return addUserToChannel(userId, event.channel);
      case 'removeUser':
        return removeUserFromChannel(userId, event.channel);
    }
  } else if (event.text.match(commandRegexp)) {
    const [, botId, command] = commandRegexp.exec(event.text)
    switch (command) {
      case 'listUsers':
        return listChannelUsers(event.channel);
      case 'purgeChannel':
        return purgeChannel(event.channel);
    }
  }

  return await bot.client.chat.postMessage({
    token: process.env.SLACK_BOT_TOKEN,
    channel: event.channel,
    text: 'No comprendo.... :/'
  });
}

function addUserToChannel(userId, channelId) {
  return await bot.client.chat.postMessage({
    token: process.env.SLACK_BOT_TOKEN,
    channel: channelId,
    text: `Todo: Add user ${userId} into channel ${channelId}`
  });
}

function removeUserFromChannel(userId, channelId) {
  return await bot.client.chat.postMessage({
    token: process.env.SLACK_BOT_TOKEN,
    channel: channelId,
    text: `Todo: remove user ${userId} from channel ${channelId}`
  });
}

function listChannelUsers(channelId) {
  return await bot.client.chat.postMessage({
    token: process.env.SLACK_BOT_TOKEN,
    channel: channelId,
    text: `Todo: List users for channel ${channelId}`
  });
}

function purgeChannel(channelId) {
  return await bot.client.chat.postMessage({
    token: process.env.SLACK_BOT_TOKEN,
    channel: channel,
    text: `Todo: Purge channel ${channelId}`
  });
}